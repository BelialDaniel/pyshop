# This is the file where django finds all routs
from django.urls import path # this function map the urls
from . import views

# path ('', method)
#  the first argument specified the path to the url endpoint
#  empty string in the first argument represent the rout of this app
#  in the second argument is especified the view fucntion
#  views.index intead of especified the function we especified the 
#  reference 

# This is a requiered variable by django
urlpatterns = [
    path('', views.index),
    path('new/', views.new)
]