from django.db import models


# Create your models here.
class Product(models.Model):
    # This class is used to especified the table name
    class Meta:
        db_table = 'products'
    
    name = models.CharField(max_length=255)
    price = models.FloatField()
    stock = models.IntegerField()
    image_url = models.CharField(max_length=2083) # standar max url length


class Offer(models.Model):
    class Meta:
        db_table = 'offers'
    
    code = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    discount = models.FloatField()

