from django.contrib import admin
from .models import Product, Offer

# Register your models here.
# This class was created to customized the products in the 
#  admin panel
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'price',
        'stock'
    )


class OfferAdmin(admin.ModelAdmin):
    list_display = (
        'code',
        'discount',
        'description'
    )


admin.site.register(Product, ProductAdmin)
admin.site.register(Offer, OfferAdmin)